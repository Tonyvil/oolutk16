DROP TABLE name;
DROP TABLE tier;
DROP TABLE recipient;
DROP TABLE package;
DROP TABLE attribute;
DROP TABLE items;
DROP TABLE open;
DROP TABLE city;
DROP TABLE smartpost;
DROP TABLE route;
DROP TABLE delivery;
DROP TABLE log;
DROP VIEW countrows;
DROP TABLE postcode;
DROP TABLE itemsinpackages;
DROP VIEW smartpostinfo;
DROP VIEW statistics;
DROP TABLE recipient;

Create Table name (
    nameid INTEGER PRIMARY KEY,
    name VARCHAR(64) UNIQUE NOT NULL
);

CREATE TABLE tier (
	tierid INTEGER PRIMARY KEY,    
	maxsize REAL NOT NULL,
    maxtravel REAL NOT NULL,
    brakes BOOLEAN DEFAULT(0)
);

CREATE TABLE recipient (
    recipientid INTEGER PRIMARY KEY,
    firstname VARCHAR(32) NOT NULL,
    lastname VARCHAR(32) NOT NULL,
    address VARCHAR(64) NOT NULL
);

Create Table attribute (
    attributeid INTEGER PRIMARY KEY,
    nameid INTEGER NOT NULL,
    weight REAL DEFAULT 1,
    size REAL DEFAULT 1,
    breakable BOOLEAN DEFAULT 0,
    CHECK (weight > 0),
    CHECK (size > 0),
    FOREIGN KEY(nameid) REFERENCES name(nameid)
);

CREATE TABLE open (
    openid INTEGER PRIMARY KEY,
    times VARCHAR(64) UNIQUE NOT NULL
);

CREATE TABLE city (
    cityid INTEGER PRIMARY KEY,
    city VARCHAR(32) UNIQUE NOT NULL
);

CREATE TABLE postcode (
	postcodeid INTEGER PRIMARY KEY,
	postcode VARCHAR(10) NOT NULL
);

CREATE TABLE smartpost (
    smartpostid INTEGER PRIMARY KEY,
    cityid INTEGER NOT NULL,
    openid INTEGER NOT NULL,    
    longitude VARCHAR(32) NOT NULL,
    latitude VARCHAR(32) NOT NULL,
    name VARCHAR(64) NOT NULL,
    address VARCHAR(32) NOT NULL,
    postcodeid INTEGER NOT NULL,
    FOREIGN KEY(postcodeid) REFERENCES postcode(postcodeid),
    FOREIGN KEY(cityid) REFERENCES city(cityid),
    FOREIGN KEY(openid) REFERENCES open(openid)
	ON DELETE CASCADE
);


CREATE TABLE route (
    routeid INTEGER PRIMARY KEY,
    distance REAL,
    destinationid INTEGER NOT NULL,
    originid INTEGER NOT NULL CHECK(originid != destinationid),
    FOREIGN KEY(destinationid) REFERENCES smartpost(smartpostid),
    FOREIGN KEY(originid) REFERENCES smartpost(smartpostid)
);

CREATE TABLE delivery (
    deliveryid INTEGER PRIMARY KEY,
    routeid INTEGER NOT NULL,
    sent BOOLEAN DEFAULT 0,
    FOREIGN KEY(routeid) REFERENCES route(routeid)
);


CREATE TABLE package (
    packageid INTEGER PRIMARY KEY,
    tierid INTEGER NOT NULL,
    recipientid INTEGER NOT NULL,
    deliveryid INTEGER NOT NULL,
    sent BOOLEAN DEFAULT 0,
    FOREIGN KEY(deliveryid) REFERENCES delivery(deliveryid),
    FOREIGN KEY(tierid) REFERENCES tier(tierid),
    FOREIGN KEY(recipientid) REFERENCES recipient(recipientid)
	ON DELETE CASCADE
);

CREATE TABLE items (
    itemid INTEGER PRIMARY KEY,
    attributeid INTEGER NOT NULL,
    FOREIGN KEY(attributeID) REFERENCES attribute(attributeid)
	
);

CREATE TABLE itemsinpackages (
    itemid INTEGER,
    packageid INTEGER,
    numitems INTEGER,
    PRIMARY KEY(itemid, packageid),
    FOREIGN KEY(itemid) REFERENCES items(itemid)
    FOREIGN KEY(packageid) REFERENCES package(packageid)
	ON DELETE CASCADE
);

CREATE TABLE log (
    logid INTEGER PRIMARY KEY,
    deliveryid INTEGER NOT NULL,
    timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY(deliveryid) REFERENCES delivery(deliveryid)
);

CREATE VIEW countrows AS
    SELECT
    (SELECT COUNT(*) FROM package) AS packagecount,
    (SELECT COUNT(*) FROM route) AS routecount,
    (SELECT COUNT(*) FROM delivery) AS deliverycount,
    (SELECT COUNT(*) FROM name) AS namecount,
    (SELECT COUNT(*) FROM attribute) AS attributecount,
    (SELECT COUNT(*) FROM items) AS itemcount,
    (SELECT COUNT(*) FROM log) AS logcount,
    (SELECT COUNT(*) FROM smartpost) AS smartpostcount,
    (SELECT COUNT(*) FROM recipient) AS recipientcount
;
CREATE VIEW statistics AS
    SELECT
    (SELECT COUNT(*) FROM package WHERE sent = 1) AS sentpackagecount,
    (SELECT COUNT(*) FROM delivery WHERE sent = 1) AS sentdeliverycount,
    (SELECT MIN(distance) FROM delivery INNER JOIN route) AS mindistance,
    (SELECT MAX(distance) FROM delivery INNER JOIN route) AS maxdistance,
    (SELECT TOTAL(distance) FROM delivery INNER JOIN route) AS distancetraveled,
    (SELECT AVG(distance) FROM delivery INNER JOIN route) AS avgdistance
;

CREATE VIEW smartpostinfo AS
    SELECT * FROM smartpost INNER JOIN open ON 
    smartpost.smartpostid = open.openid INNER JOIN
    city ON smartpost.cityid = city.cityid INNER JOIN 
    postcode ON smartpost.postcodeid = postcode.postcodeid
;

INSERT INTO tier
VALUES (1, 0, 150, 1);
INSERT INTO tier
VALUES (2, 50, 0, 0);
INSERT INTO tier
VALUES (3, 0, 0, 1);
