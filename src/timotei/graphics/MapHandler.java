/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei.graphics;

import java.util.ArrayList;
import java.util.Arrays;
import javafx.scene.web.WebView;
import timotei.dataClasses.SmartPost;

/**
 *
 * This class handles all javascript interactions.
 *
 */
public class MapHandler {

    private WebView map;
    private static MapHandler instance = null;

    private MapHandler() {
    }

    public static MapHandler getInstance() {
        if (instance == null) {
            instance = new MapHandler();
        }
        return instance;
    }

    public void setMap(WebView map) {
        this.map = map;
    }

    public void drawPost(SmartPost post) {//draw a smartpost using Lat and Lng
        double lat = Double.parseDouble(post.getLat());
        double lng = Double.parseDouble(post.getLng());
        String info = post.getName();
        map.getEngine().executeScript("document.createMarker('" + lat + "', '" + lng + "', '" + info + "','red')");
    }

    public void drawMultiplePosts(ArrayList<SmartPost> posts) {
        for (SmartPost smartPost : posts) {  //draw posts form an array list
            drawPost(smartPost);
        }
    }

    public void deletePosts() {//clear the map of markers
        map.getEngine().executeScript("document.deleteMarkers()");
    }

    public double calcDistance(SmartPost origin, SmartPost destination) {
        //BUGI Pakettiautomaatti, Kauppakeskus Espoontori ->  Pakettiautomaatti, S-market Elimäki = -1
        ArrayList<String> a = new ArrayList();
        a.addAll(Arrays.asList(origin.getLat(), origin.getLng(), destination.getLat(), destination.getLng()));
        drawPost(origin);
        drawPost(destination);
        Object len = map.getEngine().executeScript("document.pathLength(" + a + ")");
        double toReturn = -1;
        if (len instanceof Double) {
            toReturn = (double) len;
        } else if (len instanceof Integer) {
            Integer temp = (Integer) len;
            toReturn = temp.doubleValue();
        }
        return toReturn;
    }

    public void drawRoute(SmartPost origin, SmartPost destination, int tier) {
        ArrayList<String> route = new ArrayList();//draw a route
        route.addAll(Arrays.asList(origin.getLat(), origin.getLng(), destination.getLat(), destination.getLng()));
        drawPost(origin);
        drawPost(destination);
        map.getEngine().executeScript("document.createPath(" + route + ", 'blue', '" + tier + "')");
    }

    public void deleteRoutes() {//clear the map of paths
        map.getEngine().executeScript("document.deletePaths()");
    }
}
