package timotei.database;

import timotei.dataClasses.SmartPost;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import static java.util.Collections.sort;
import java.util.logging.Level;
import java.util.logging.Logger;
import timotei.graphics.MapHandler;

/**
 *
 * @author Tony Vilpponen 0452774
 */
/**
 * This objects handles SmartPost data.
 */
public final class SmartPostHandler {

    private static SmartPostHandler instance = null;
    private DatabaseHandler databaseHandler;
    private MapHandler mapHandler;
    private SmartPostHandler() {
        databaseHandler = DatabaseHandler.getInstance();
        mapHandler = MapHandler.getInstance();
    }

    public static SmartPostHandler getInstance() {
        if (instance == null) {
            instance = new SmartPostHandler();
        }
        return instance;
    }
    public int getSmartPostCount() {
        return databaseHandler.querySmartPostCount();
    }


    public ArrayList<SmartPost> getSmartPostsByCity(String city) {
        ArrayList<SmartPost> toReturn = new ArrayList();
        ResultSet results = databaseHandler.querySmartPosts(city);

        try {
            if (results.isBeforeFirst()) {
                while (results.next()) {//we add relevant data to the SmartPost objects and add them to the list
                    toReturn.add(new SmartPost(results.getString("longitude"),
                            results.getString("latitude"), results.getString("name")));
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(SmartPostHandler.class.getName()).log(Level.OFF, null, ex);
            return null;
        }//we return the list
        return toReturn;
    }
    
    public ArrayList<String> getSmartPostNames(){
        ArrayList<String> toReturn = new ArrayList();
        ResultSet posts = databaseHandler.querySmartPosts("%");
        try {//get all the smartpost names "%" is a wildcard that corresponds to everything in the database
            if(posts != null && posts.isBeforeFirst()){
                while(posts.next()){//add name of the smartpost
                    toReturn.add(posts.getString(3));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(SmartPostHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return toReturn;
    }
    
    public SmartPost getPostByName(String name){
        ResultSet query = databaseHandler.queryPost(name);
        try {//use a smartposts name to get information.
            if(query != null && query.isBeforeFirst()){
             query.next();
             SmartPost toReturn = new SmartPost(query.getString("longitude"),
                     query.getString("latitude"), query.getString("times"),
                     query.getString("postcode"), query.getString("city"),
                     query.getString("address"), query.getString("name"));
             toReturn.setId(query.getInt("smartpostid"));
             return toReturn;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SmartPostHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public void drawRouteByRouteid(int routeid, int tier){
        ResultSet query = databaseHandler.queryRouteInfo(routeid);
        try { //draw a route on the map
            if(query != null && query.isBeforeFirst()){
                query.next();
                String destinat = query.getString("destination");
                SmartPost origin = getPostByName(query.getString("origin"));
                SmartPost destination = getPostByName(destinat);
                mapHandler.drawRoute(origin, destination, tier);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SmartPostHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<String> getCities() {
        ArrayList toReturn = new ArrayList();
        ResultSet query = databaseHandler.queryCities();
        try {//get all the cities that contain smartposts
            if (query.isBeforeFirst()) {
                while (query.next()) {
                    toReturn.add(query.getString(1));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        sort(toReturn);
        return toReturn;
    }

    public void emptyStorage(){ //deletes unsent packages
        databaseHandler.loadNewStorage();
    }
}
