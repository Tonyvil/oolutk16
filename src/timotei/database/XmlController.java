package timotei.database;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import timotei.dataClasses.SmartPost;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 *
 * @author Tony Vilpponen 0452774
 */

/**
 * This object loads the needed data for SmartPost from the internet.
 */
public class XmlController {

    private Document doc;

    public String getXmlDocument() {
        //Here we download the xml document from the internet.
        String toReturn = "";
        try {
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String inputLine;
            
            while ((inputLine = in.readLine()) != null) {
                toReturn += inputLine;
            }
            in.close();
        } catch (MalformedURLException ex) {
            Logger.getLogger(XmlController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XmlController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return toReturn;
    }

    public void buildXML(String total) {
        try {
            //Here we build the XML document for further use.
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            doc = dBuilder.parse(new InputSource(new StringReader(total)));
            doc.getDocumentElement().normalize();

        } catch (SAXException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(XmlController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<SmartPost> parseXML() {
        //Here we make SmartPost objects from the data within the XML file
        ArrayList<SmartPost> toReturn = new ArrayList();
        NodeList nl = doc.getElementsByTagName("place");
        for (int i = 0; i < nl.getLength(); i++) {
            Node node = nl.item(i);
            Element ele = (Element) node;
            //we remove excess whitespace from the entries.
            String postCode = ele.getElementsByTagName("code").item(0).getTextContent().trim();
            String city = ele.getElementsByTagName("city").item(0).getTextContent().trim();
            String adress = ele.getElementsByTagName("address").item(0).getTextContent().trim();
            String openHours = ele.getElementsByTagName("availability").item(0).getTextContent().trim();
            String lat = ele.getElementsByTagName("lat").item(0).getTextContent().trim();
            String lng = ele.getElementsByTagName("lng").item(0).getTextContent().trim();
            String name = ele.getElementsByTagName("postoffice").item(0).getTextContent().trim();
            toReturn.add(new SmartPost(lng, lat, openHours, postCode, city, adress, name));
        }
        //And return an ArrayList for further processing.
        return toReturn;
    }
}
