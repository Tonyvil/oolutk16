
package timotei.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import timotei.dataClasses.Item;

/**
 *
 * This class handles events that deal with items
 */
public class ItemHandler {

    private DatabaseHandler dh;
    private ArrayList<Item> items;
    private static ItemHandler instance = null;

    public static ItemHandler getInstance() {
        if(instance == null){
            instance = new ItemHandler();
        }
        return instance;
    }
    
    private ItemHandler() {
        dh = DatabaseHandler.getInstance();
        items = new ArrayList();
    }

    public void addItems(Item item) {
        this.items.add(item);
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void addItem(Item item) {
        int nameid = addName(item.getName()); //add an item to the database
        int attributeid = addAttribute(item.getWeight(), item.getSize(), item.isBreakable(), nameid);
        if (nameid == -1 || attributeid == -1) {
            Logger.getLogger(ItemHandler.class.getName()).log(Level.SEVERE, null, new SQLException("Name id was " + nameid + " "
                    + "and attribute id was " + attributeid + "When they should be > 0"));
        }
        int itemid = dh.addItem(attributeid);
        item.setItemid(itemid);
        addItems(item);
    }

    private int addAttribute(double weight, double size, boolean breakable, int nameid) {
        if (nameid == -1) {
            return -1;
        }//add an attribute to the database and return the id
        return dh.addAttribute(nameid, weight, size, breakable);
    }

    private int addName(String name) {//add a name to the database and return the id
        ResultSet query = dh.getName(name);
        try {
            if (query != null && query.isBeforeFirst()) {
                query.next();
                return query.getInt(1);
            } else if (query != null) {
                query.close();
                return dh.addName(name);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ItemHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;//if all fails return -1 so all the other methods know something went wrong
    }

    public double calculateWeight(ArrayList<Item> items) {
        double weight = 0;//calculate the weight of items in an array list
        for (Item item2 : items) {
            weight += item2.getWeight();
        }
        return weight;
    }

    public ArrayList<Item> loadItems() {//get items from the database
        ResultSet query = dh.queryItems();
        items.clear();
        try {
            if (query.isBeforeFirst()) {
                while(query.next()){
                    addItems(new Item(query.getString("name"), query.getDouble("weight"), query.getDouble("size"),query.getBoolean("breakable")));
                }
                return items;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ItemHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList();
    }

    public double calculateSize(ArrayList<Item> items) {
        double size = 0;//calculate the size of items in an array list
        for (Item item : items) {
            size += item.getSize();
        }
        return size;
    }

}
