package timotei.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import timotei.graphics.MapHandler;

/**
 *
 * This class handles generic queries to the database
 */
public class QueryHandler {

    private DatabaseHandler databaseHandler;
    private SmartPostHandler sph;
    private MapHandler mh;

    public QueryHandler() {
        databaseHandler = DatabaseHandler.getInstance();
        sph = SmartPostHandler.getInstance();
        mh = MapHandler.getInstance();
    }

    public double getRouteLength(String originName, String destinationName) {
        int origin = sph.getPostByName(originName).getId();
        int destination = sph.getPostByName(destinationName).getId();
        ResultSet query = databaseHandler.queryRouteDistance(destination, origin);
        double distance = -1;
        try {//we try to get the distance from the database
            if (query.isBeforeFirst()) {
                if (query.getDouble("distance") == 0) {//if the distance is 0
                    distance = mh.calcDistance(sph.getPostByName(originName),
                            sph.getPostByName(destinationName));//calculate the distance
                    databaseHandler.updateRouteDistance(query.getInt("routeid"), distance);//add it to the database
                } else { //get the already calculated distance and return it
                    distance = query.getDouble("distance");
                }
                return distance;
            } else { //this means the route is not in the database yet
                distance = mh.calcDistance(sph.getPostByName(originName),
                        sph.getPostByName(destinationName));
                databaseHandler.addRoute(destination, origin, distance);//so we add a route and calculate its distance
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }//return the distance
        return distance;
    }

    public double getRouteLengthById(int routeid) {
        double distance = 0;//get the lenght of a route by its id
        ResultSet query = databaseHandler.queryRouteInfo(routeid);
        try {
            if (query.isBeforeFirst()) {
                distance = query.getDouble("distance");
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return distance;
    }

    public HashMap<String, Integer> getDeliveries() {
        HashMap<String, Integer> toReturn = new HashMap();
        ResultSet query;
        int routecount = databaseHandler.queryRouteCount();
        for (int i = 1; i <= routecount; i++) {
            query = databaseHandler.queryRouteInfo(i);
            try {
                if (query.isBeforeFirst()) {//add the routeinfo of the deliveries
                    toReturn.put(query.getString("origin") + " -> " + query.getString("destination"), i);
                }
            } catch (SQLException ex) {
                Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return toReturn;
    }

    public ArrayList<String> getPacketsByRoute(int routeid, int tier) {
        ArrayList<String> toReturn = new ArrayList();//add packets that have not been sent by route
        ResultSet query = databaseHandler.queryPacketsByRoute(routeid);
        boolean checkForTier = tier != 0;//set a flag for checking tier(because we use the same function even after
        try {//some packets have been added to the delivery
            if (query.isBeforeFirst()) {
                while (query.next()) {
                    if (query.getBoolean("sent")) {
                        continue;
                    }
                    if (checkForTier && query.getInt("tierid") != tier) {
                        continue;
                    }
                    toReturn.add(query.getString(1) + ": " + query.getString(2) + " " + query.getString(3) + " " + query.getInt("tierid"));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return toReturn; //return a list of the deliveries
    }

    public int getPacketNumber() {
        return databaseHandler.queryPacketNumber();
    }

}
