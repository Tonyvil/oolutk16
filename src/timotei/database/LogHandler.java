package timotei.database;

/**
 *
 * This class handles log events
 */
public class LogHandler {

    private DatabaseHandler dbh;

    private int createdPackets;
    private int sentPackets;
    private int storedPackets;
    private int deliveriesMade;
    private double shortestDistance;
    private double longestDistance;
    private double combinedDistance;
    private static LogHandler instance = null;

    private LogHandler() {
        createdPackets = 0;
        sentPackets = 0;
        deliveriesMade = 0;
        shortestDistance = 0;
        longestDistance = 0;
        combinedDistance = 0;
        dbh = DatabaseHandler.getInstance();
        storedPackets = getStoredPackets(true);
    }

    public static LogHandler getInstance() {
        if (instance == null) {
            instance = new LogHandler();
        }
        return instance;
    }

    public void addPacket() {
        createdPackets++;
        storedPackets++;
    }

    public void sendPackets(int amount) {
        sentPackets += amount;
        storedPackets -= amount;
    }

    public void makeDelivery(double distance) {
        if (distance < shortestDistance || shortestDistance == 0) {
            shortestDistance = distance;
        }
        if (distance > longestDistance) {
            longestDistance = distance;
        }
        combinedDistance += distance;
        deliveriesMade++;
    }
//these classes return data based on if the data wanted is from this session or
    //accross all sessions
    public int getCreatedPackets(boolean alltime) {

        if (!alltime) {
            return createdPackets;
        }
        return dbh.queryPacketNumber();
    }

    public final int getStoredPackets(boolean alltime) {
        if (!alltime) {
            return storedPackets;
        }
        return dbh.queryPacketNumber() - (int) dbh.getStatistic("sentpackagecount");
    }

    public int getSentPackets(boolean alltime) {
        if (!alltime) {
            return sentPackets;
        }
        return getCreatedPackets(alltime) - getStoredPackets(alltime);
    }

    public int getDeliveriesMade(boolean alltime) {
        if (!alltime) {
            return deliveriesMade;
        }
        return dbh.queryDeliveryNumber();
    }

    public double getPacketsDelivery(boolean alltime) {
        if (!alltime) {
            if (deliveriesMade == 0) {
                return 0;
            }
            return Math.round(((double) createdPackets / deliveriesMade) * 100) / 100;
        }
        int alldeliv = getDeliveriesMade(alltime);
        if (alldeliv == 0) {
            return 0;
        }
        return Math.round(((double) getCreatedPackets(alltime) / alldeliv) * 100) / 100;
    }

    public double getShortestDistance(boolean alltime) {
        double distance = dbh.getStatistic("mindistance");
        if (!alltime) {
            return shortestDistance;
        }
        return distance;
    }

    public double getLongestDistance(boolean alltime) {
        double distance = dbh.getStatistic("maxdistance");
        if (!alltime) {
            return longestDistance;
        }
        return distance;
    }

    public double getAverageDistance(boolean alltime) {
        double distance = dbh.getStatistic("avgdistance");
        if (!alltime) {
            if (deliveriesMade == 0 || combinedDistance == 0) {
                return 0;
            }
            return Math.round(combinedDistance / deliveriesMade * 100) / 100;
        }
        return Math.round(distance * 100) / 100;
    }

    public double getAllDistance(boolean alltime) {
        if (!alltime) {
            return Math.round(combinedDistance * 100) / 100;
        }
        return Math.round(dbh.getStatistic("distancetraveled") * 100) / 100;
    }
}
