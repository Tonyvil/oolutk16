package timotei.database;

import timotei.dataClasses.SmartPost;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tony Vilpponen 0452774
 */
/**
 * This object handles all database related actions
 */

public class DatabaseHandler {

    private Connection connection;
    private static DatabaseHandler instance = null;
    private PreparedStatement pps;

    private DatabaseHandler() {

    }

    public static DatabaseHandler getInstance() {
        if (instance == null) {
            instance = new DatabaseHandler();
        }
        return instance;
    }

    public void loadNewStorage() {
        //we delete all unsent packages
        try {
            connection = makeConnection();
            pps = connection.prepareStatement("DELETE FROM package WHERE sent = 0");
            pps.execute();
            pps.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void addCity(ArrayList<SmartPost> list) {
        try {
            connection = makeConnection();
            int id = 0;
            pps = connection.prepareStatement("INSERT INTO city VALUES(?,?);");
            String latestCity = ""; //we start going through the list of smartboxes
            for (SmartPost smp : list) {//and save the last city we saved
                try {

                    pps.setInt(1, id);
                    if (smp.getCity().equals(latestCity)) { //check if the city is
                        continue; //the same as before and skip if it is
                    }
                    pps.setString(2, smp.getCity());
                    latestCity = smp.getCity();
                    pps.executeUpdate(); //Send the insert command to the database
                    id++;
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage() + " While inserting cities.");
                }
            }
            pps.close();
            connection.close();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void addPostCode(ArrayList<SmartPost> list) {
        try {
            connection = makeConnection();
            ArrayList<String> values = new ArrayList();
            //add postcodes to the database
            pps = connection.prepareStatement("INSERT INTO postcode (postcode) VALUES(?)");
            for (SmartPost smartPost : list) {
                if (values.contains(smartPost.getPostCode())) {
                    continue;
                }
                pps.setString(1, smartPost.getPostCode());
                pps.executeUpdate();
                values.add(smartPost.getPostCode());
            }
            pps.close();
            connection.close();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void addOpenHour(ArrayList<SmartPost> list) {
        try {
            ArrayList<String> alreadyAdded = new ArrayList();
            connection = makeConnection(); //We have to save the latest entries
             //to a list because they are not in order
            pps = connection.prepareStatement("INSERT INTO open (times) VALUES(?);");
            for (SmartPost smp : list) {
                try {
                    if (alreadyAdded.contains(smp.getOpen())) {
                        continue;//Check if we already added the open times
                    }
                    pps.setString(1, smp.getOpen());
                    alreadyAdded.add(smp.getOpen());
                    pps.executeUpdate();//Send the insert command to the database
                    
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage() + " While inserting open hours");
                }
            }
            pps.close();
            connection.close();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void addSmartBoxes(ArrayList<SmartPost> list) {
        try {
            connection = makeConnection();//Prepare all the statements
            PreparedStatement cityQ = connection.prepareStatement("SELECT cityid FROM city WHERE city LIKE ?");
            PreparedStatement openQ = connection.prepareStatement("SELECT openid FROM open WHERE times LIKE ?");
            PreparedStatement postcQ = connection.prepareStatement("SELECT postcodeid FROM postcode WHERE postcode LIKE ?");
            int id = 0;
            for (SmartPost sm : list) {
                try {
                    cityQ.setString(1, sm.getCity());
                    int city = cityQ.executeQuery().getInt("cityid");
                    openQ.setString(1, sm.getOpen());//Query the foreign keys
                    int open = openQ.executeQuery().getInt("openid");
                    postcQ.setString(1, sm.getPostCode());
                    int postnum = postcQ.executeQuery().getInt("postcodeid");
                    pps = connection.prepareStatement("INSERT INTO smartpost VALUES(?,?,?,?,?,?,?,?)");
                    pps.setInt(1, id);
                    pps.setInt(2, city);
                    pps.setInt(3, open);
                    pps.setString(4, sm.getLng());
                    pps.setString(5, sm.getLat());
                    pps.setString(6, sm.getName());
                    pps.setString(7, sm.getAddress());
                    pps.setInt(8, postnum);
                    pps.executeUpdate();
                    id++;
                } catch (SQLException e) {
                    System.err.println(e.getMessage() + " While inserting SmartBoxes");
                    Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, e);
                    return;
                }
            }
            cityQ.close();
            openQ.close();
            pps.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public int addItem(int attributeid) {
        try {
            int itemid;
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT itemid FROM items WHERE "
                    + " attributeid = ?");
            pps.setInt(1, attributeid);
            ResultSet itemQ = pps.executeQuery();
            if (itemQ.isBeforeFirst()) {
                itemid = itemQ.getInt("itemid");
            } else {
                pps.close();
                itemid = getRowCount("itemcount"); //get new id for the item
                connection = makeConnection();
                pps = connection.prepareStatement("INSERT INTO items VALUES(?,?)");
                pps.setInt(1, itemid);
                pps.setInt(2, attributeid);
                pps.executeUpdate();
            }
            pps.close();
            connection.close();
            return itemid; //return the id we got from the database
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public void addItemsToPackage(int itemid, int packageid) {
        try {
            connection = makeConnection();//Check if the item is already in the package
            pps = connection.prepareStatement("SELECT numitems FROM itemsinpackages "
                    + " WHERE itemid = ? AND packageid = ?");
            pps.setInt(1, itemid);
            pps.setInt(2, packageid);
            ResultSet query = pps.executeQuery();
            if (query.isBeforeFirst()) {
                pps.close();//if the item is in the package, raise count by 1
                pps = connection.prepareStatement("UPDATE itemsinpackages SET "
                        + " numitems = numitems + 1");
                pps.executeUpdate();
            } else {
                pps.close();//else add the item into the package
                pps = connection.prepareStatement("INSERT INTO itemsinpackages "
                        + " VALUES(?,?,?)");
                pps.setInt(1, itemid);
                pps.setInt(2, packageid);
                pps.setInt(3, 1);
                pps.execute();
            }
            pps.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int addAttribute(int nameid, double weight, double size, boolean breakable) {
        try {
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT attributeid FROM attribute WHERE "
                    + " nameid = ? AND weight = ? AND size = ? AND breakable = ?");
            pps.setInt(1, nameid);
            pps.setDouble(2, weight);
            pps.setDouble(3, size);
            pps.setBoolean(4, breakable);
            ResultSet query = pps.executeQuery();
            if (query.isBeforeFirst()) {
                query.next(); //return the id of the matching attribute
                return query.getInt(1);
            }
            pps.close();
            int attributeid = getRowCount("attributecount");
            connection = makeConnection(); //create and return the id of the new attribute
            pps = connection.prepareStatement("INSERT INTO attribute VALUES(?,?,?,?,?)");
            pps.setInt(1, attributeid);
            pps.setInt(2, nameid);
            pps.setDouble(3, weight);
            pps.setDouble(4, size);
            pps.setBoolean(5, breakable);
            pps.executeUpdate();
            pps.close();
            connection.close();
            return attributeid;
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public int querySmartPostCount() {
        return getRowCount("smartpostcount");
    }

    public ResultSet getName(String name) {
        try {
            connection = makeConnection();//get the id of an items name
            pps = connection.prepareStatement("SELECT nameid FROM name WHERE name LIKE ?");
            pps.setString(1, name);
            return pps.executeQuery();
        } catch (SQLException ex) {

            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int addName(String name) {
        int nameid = -1;
        try {
            ResultSet query = getName(name);
            if (query.isBeforeFirst()) {
                query.next();
                return query.getInt(1);
            }

            nameid = getRowCount("namecount");//add a name of an item to the database
            connection = makeConnection();
            pps = connection.prepareStatement("INSERT INTO name VALUES(?,?)");
            pps.setInt(1, nameid);
            pps.setString(2, name);
            pps.executeUpdate();
            pps.close();
            connection.close();
        } catch (SQLException e) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, e);
        }
        return nameid;
    }

    public int addPackage(int tierid, int recipientid, int deliveryid) {
        int packageid = -1;
        try {
            packageid = getRowCount("packagecount");
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT packageid FROM package WHERE tierid = ? "
                    + " AND recipientid = ? AND deliveryid = ? AND sent = 0");
            pps.setInt(1, tierid);
            pps.setInt(2, recipientid);
            pps.setInt(3, deliveryid);
            ResultSet query = pps.executeQuery();//check if package is already in the database
            if (query.isBeforeFirst()) {
                query.next();
                packageid = query.getInt(1);
                return packageid;//return the id of the found package
            }//else create a new one
            connection = makeConnection();
            pps = connection.prepareStatement("INSERT INTO package (tierid, recipientid, "
                    + " deliveryid) VALUES(?,?,?)");
            pps.setInt(1, tierid);
            pps.setInt(2, recipientid);
            pps.setInt(3, deliveryid);
            pps.executeUpdate();
            pps.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage() + "while adding package");
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, e);
        }
        return packageid;
    }

    public int addDelivery(int destinationID, int originID) {
        int routeid;
        int deliveryid = -1;
        try {
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT routeid FROM route "
                    + " WHERE destinationid = ? AND originid = ?");
            pps.setInt(1, destinationID);
            pps.setInt(2, originID);
            ResultSet deliveryQ = pps.executeQuery();
            if (deliveryQ.isBeforeFirst()) {
                routeid = deliveryQ.getInt(1);//If route is already in the database use that id
            } else {//otherwise make a new route
                routeid = addRoute(destinationID, originID, 0);
                //Create a new route using the destination and
                //origin ids
            }
            pps.close();
            pps = connection.prepareStatement("SELECT deliveryid FROM delivery WHERE routeid "
                    + " = ?");
            pps.setInt(1, routeid);
            ResultSet query = pps.executeQuery();
            if (query.isBeforeFirst()) {
                query.next();
                deliveryid = query.getInt(1);
                pps.close();
                connection.close();
                return deliveryid;
            }
            connection = makeConnection();//and add this one as last
            pps = connection.prepareStatement("INSERT INTO delivery (routeid)"
                    + " VALUES(?)");
            pps.setInt(1, routeid);
            pps.executeUpdate();//Create the delivery to the database
            pps.close();
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT deliveryid FROM delivery WHERE routeid = ?");
            pps.setInt(1, routeid);
            ResultSet delivery = pps.executeQuery();
            if (delivery.isBeforeFirst()) {
                delivery.next();
                deliveryid = delivery.getInt(1);
            }
            pps.close();
            connection.close();
        } catch (SQLException e) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, e);

        }
        return deliveryid;
    }

    public void writeLog(int deliveryID) {
        try {//create a new log entry
            connection = makeConnection();
            pps = connection.prepareStatement("INSERT INTO log (deliveryid) VALUES (?)");
            pps.setInt(1, deliveryID);
            pps.executeUpdate();
            connection.close();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public int addRoute(int destinationID, int originID, double len) {
        int routeid = -1;
        try {
            routeid = getRowCount("routecount");
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT routeid, distance FROM route WHERE originid = ? "
                    + " AND destinationid = ?");
            pps.setInt(1, originID);
            pps.setInt(2, destinationID);
            ResultSet query = pps.executeQuery();
            if (query.isBeforeFirst()) {
                query.next();//check if the route already exists
                routeid = query.getInt(1);
                pps.close();//update the distance if it differs from the value
                if (len != query.getDouble("distance")) {//in the database
                    updateRouteDistance(routeid, len);
                }//if yes return id
                return routeid;
            }//if not create it and return id
            pps = connection.prepareStatement("INSERT INTO route (distance, destinationid, originid) "
                    + " VALUES(?,?,?)");
            pps.setDouble(1, len);
            pps.setInt(2, destinationID);
            pps.setInt(3, originID);
            pps.executeUpdate();
            pps.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return routeid;
    }

    public ResultSet queryRouteDistance(int destinationID, int originID) {
        try {
            connection.close();
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT routeid, distance FROM route "
                    + " WHERE destinationid = ? AND originid = ? ");
            pps.setInt(1, destinationID);
            pps.setInt(2, originID);//get the distance of a route
            return pps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void updateRouteDistance(int routeid, double len) {
        try {
            connection = makeConnection();
            pps = connection.prepareStatement("UPDATE route SET distance = ? WHERE routeid = ?");
            pps.setInt(2, routeid);
            pps.setDouble(1, len);//update the distance of a route
            pps.executeUpdate();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ResultSet queryPost(String name) {
        try {
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT * FROM smartpost INNER JOIN "
                    + " postcode ON smartpost.postcodeid = postcode.postcodeid INNER JOIN "
                    + " city ON smartpost.cityid = city.cityid INNER JOIN "
                    + " open ON smartpost.openid = open.openid "
                    + " WHERE smartpost.name LIKE ?");
            pps.setString(1, name);//get info on a smartpost
            return pps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ResultSet queryCities() {
        try {
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT DISTINCT city "
                    + " FROM city");//get all the cities in the database
            return pps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ResultSet querySmartPosts(String city) {
        try {
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT latitude, longitude, name "
                    + " FROM smartpost "
                    + " INNER JOIN city ON city.cityid = smartpost.cityid "
                    + " WHERE city LIKE ?");
            pps.setString(1, city);//get smartposts based on a city
            return pps.executeQuery();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public ResultSet queryDeliveries() {
        try {
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT * FROM delivery");
            return pps.executeQuery();
        } catch (SQLException ex) {//get all the deliveries
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ResultSet queryPacketsByRoute(int routeid) {
        try {//get all the packets based on a route
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT packageid, firstname, lastname, package.sent, tierid FROM package "
                    + " INNER JOIN recipient ON recipient.recipientid = package.recipientid "
                    + " INNER JOIN delivery ON delivery.deliveryid = package.deliveryid "
                    + " WHERE delivery.routeid = ?");
            pps.setInt(1, routeid);
            return pps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public int queryRouteCount() { //return the number of routes
        return getRowCount("routecount");
    }

    public int queryPacketDelivery(int packageid) {
        int deliveryid = -1;
        try {//get a delivery based on a packageid
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT deliveryid FROM package WHERE packageid = ?");
            pps.setInt(1, packageid);
            ResultSet query = pps.executeQuery();
            if (query.isBeforeFirst()) {
                query.next();
                deliveryid = query.getInt(1);
            }
            pps.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return deliveryid;
    }

    public boolean sendDelivery(ArrayList<String> packages) {
        ArrayList<Integer> packageids = new ArrayList();
        int deliveryid = 0;//run the neccesary updates to make it apparent that 
        boolean itembreak = false;//a delivery has been sent
        for (String string : packages) {
            int packageid = Integer.parseInt(string.split(":")[0]);
            packageids.add(packageid);
            if (containsBreakable(packageid)) {
                itembreak = true;
            }//check if the items should brake
        }
        deliveryid = queryPacketDelivery(packageids.get(0));
        setSentDelivery(deliveryid);
        //set a flag that the delivery has been sent
        for (Integer packageid : packageids) {
            sendPackage(packageid);
            try {
                int tempDeliveryid = queryPacketDelivery(packageid);
                if (tempDeliveryid == deliveryid) {
                    continue;
                }
                connection = makeConnection();//make the packages point to the sent delvery just in case
                pps = connection.prepareStatement("UPDATE package SET deliveryid = ? WHERE "
                        + " packageid = ?");
                pps.setInt(1, deliveryid);
                pps.setInt(2, packageid);
                pps.executeUpdate();
                pps.close();//delete the old delivery if it exists just in case
                pps = connection.prepareStatement("DELETE FROM delivery WHERE deliveryid = ?");
                pps.setInt(1, tempDeliveryid);
                pps.execute();
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return itembreak;//return a flag that tells if any items broke
    }

    private boolean containsBreakable(int packageid) {
        try {
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT breakable, brakes FROM itemsinpackages INNER JOIN "
                    + " items ON itemsinpackages.itemid = items.itemid INNER JOIN attribute ON "
                    + " attribute.attributeid = items.attributeid INNER JOIN package "
                    + " ON package.packageid = itemsinpackages.packageid INNER JOIN "
                    + " tier ON tier.tierid = package.tierid "
                    + " WHERE itemsinpackages.packageid = ?");
            pps.setInt(1, packageid); //check if a package has items that will break
            ResultSet query = pps.executeQuery();
            if (query.isBeforeFirst()) {
                while (query.next()) {
                    if (query.getBoolean("brakes")) {//check if the package even brakes items
                        if (query.getBoolean("breakable")) {
                            return true;
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private void setSentDelivery(int deliveryid) {
        try {
            connection = makeConnection(); //set a flag that indicates a sent delivery
            pps = connection.prepareStatement("UPDATE delivery SET sent = ? WHERE deliveryid = ?");
            pps.setBoolean(1, true);
            pps.setInt(2, deliveryid);
            pps.executeUpdate();
            pps.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void sendPackage(int packageid) {
        try {//set a flag that indicates a sent delivery
            connection = makeConnection();
            pps = connection.prepareStatement("UPDATE package SET sent = ? WHERE packageid = ?");
            pps.setBoolean(1, true);
            pps.setInt(2, packageid);
            pps.executeUpdate();
            pps.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ResultSet queryRouteInfo(int routeid) {
        try {//get information on a route
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT * FROM route, "
                    + " (SELECT name AS origin FROM route INNER JOIN smartpost ON smartpostid = "
                    + " originid WHERE routeid = ?), (SELECT name as destination FROM route INNER JOIN "
                    + " smartpost ON smartpostid = destinationid WHERE routeid = ?) "
                    + " WHERE routeid = ?");
            pps.setInt(1, routeid);
            pps.setInt(2, routeid);
            pps.setInt(3, routeid);
            return pps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ResultSet queryPackets() {
        try {//get all the packets
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT packageid, recipient.firstname, "
                    + " recipient.lastname FROM package INNER JOIN "
                    + " recipient ON package.recipientid = recipient.recipientid");
            return pps.executeQuery();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage() + " while getting packages");
        }
        return null;
    }

    //Possible columnNames are: smartpost-,package-,route-,delivery-,name-,attribute- and itemcount
    private int getRowCount(String columnName) {
        int count = -1;
        try {
            if (connection == null || connection.isClosed()) {
                connection = makeConnection();
            }
            pps = connection.prepareStatement("SELECT * FROM countrows");
            count = pps.executeQuery().getInt(columnName);
            pps.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage() + " while getting row count");
        }
        return count;
    }

    public double getStatistic(String columnName) {
        double count = 0;
        try { //get a specified statistic from the view in the database
            if (connection == null || connection.isClosed()) {
                connection = makeConnection();
            }
            pps = connection.prepareStatement("SELECT * FROM statistics");
            count = pps.executeQuery().getDouble(columnName);
            pps.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    private Connection makeConnection() throws SQLException {
        try {//connect to the database
            if (connection != null) {
                connection.close();
            }
            return DriverManager.getConnection("jdbc:sqlite:timotei.db");
        } catch (SQLException e) {
            if (connection != null) {
                connection.close();
            }
            throw new SQLException("Could not make a connection to the database");
        }
    }

    public ResultSet queryItems() {
        try {//get all the items
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT * FROM items INNER JOIN attribute ON "
                    + " attribute.attributeid = items.attributeid INNER JOIN name ON "
                    + " attribute.nameid = name.nameid");
            return pps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int queryPacketNumber() {
        return getRowCount("packagecount");
    }

    public int addRecipient(String firstname, String lastname, String address) {
        int recipientid = -1;
        try {//add a recipient to the database
            connection = makeConnection();
            pps = connection.prepareStatement("SELECT recipientid FROM recipient WHERE "
                    + " firstname LIKE ? AND lastname LIKE ? and address LIKE ?");
            pps.setString(1, firstname);
            pps.setString(2, lastname);
            pps.setString(3, address);
            ResultSet query = pps.executeQuery();
            if (query.isBeforeFirst()) {
                query.next();
                recipientid = query.getInt(1);
                pps.close();
                connection.close();
                return recipientid;
            }
            pps.close();
            recipientid = getRowCount("recipientcount");
            connection = makeConnection();
            pps = connection.prepareStatement("INSERT INTO recipient VALUES (?,?,?,?)");
            pps.setInt(1, recipientid);
            pps.setString(2, firstname);
            pps.setString(3, lastname);
            pps.setString(4, address);
            pps.executeUpdate();
            pps.close();
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return recipientid;
    }

    int queryDeliveryNumber() {
        return getRowCount("deliverycount");
    }

}
