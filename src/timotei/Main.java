package timotei;

import java.sql.SQLException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import timotei.windows.LoadOptionFXMLController;

/**
 *
 * Tony Vilpponen 0452774
 */
public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        MainLogic controller = MainLogic.getInstance();//open the main window but do not show it yet
        FXMLLoader mapWindow = new FXMLLoader(getClass().getResource("FXMLDocument.fxml"));
        Parent root = (Parent) mapWindow.load();
        Scene scene = new Scene(root);
        stage.setTitle("Map screen");
        stage.setScene(scene);
        
        //open the begginning dialog screen
        FXMLLoader loadWindow = new FXMLLoader(getClass().getResource("windows/LoadOptionFXML.fxml"));
        Parent root2 = (Parent) loadWindow.load();
        Stage stage2 = new Stage();
        Scene scene2 = new Scene(root2);
        LoadOptionFXMLController a = loadWindow.getController();
        a.setStage(stage2);//give the dialog stage to itself so we can close the widnow
        stage2.setScene(scene2);
        a.setMainWindow(stage);//give the stage of the map window to the diag screen so we can show it
        a.setFXMLDocumentController(mapWindow.getController());
        stage2.initStyle(StageStyle.UNDECORATED);
        stage2.show();
    }

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        launch(args);
    }

}
