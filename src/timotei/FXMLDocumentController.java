package timotei;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import timotei.windows.DeliveryCreationFXMLController;
import timotei.windows.PacketCreationFXMLController;

/**
 *
 * @author Tony Vilpponen 0452774
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private WebView mapDisplay;
    @FXML
    private Pane pane;
    @FXML
    private ComboBox<String> placeChoice;
    @FXML
    private TabPane tabPane;
    @FXML
    private Tab mapTab;
    @FXML
    private Tab logTab;
    @FXML
    private Label createdPackets;
    @FXML
    private Label storedPackets;
    @FXML
    private Label sentPackets;
    @FXML
    private Label deliveriesMade;
    @FXML
    private Label packetsPerDelivery;
    @FXML
    private Label shortestDistance;
    @FXML
    private Label longestDistance;
    @FXML
    private Label avgDistance;
    @FXML
    private Label combinedDistance;
    @FXML
    private Label createdPacketstotal;
    @FXML
    private Label storedPacketstotal;
    @FXML
    private Label sentPacketstotal;
    @FXML
    private Label deliveriesMadetotal;
    @FXML
    private Label packetsPerDeliverytotal;
    @FXML
    private Label shortestDistancetotal;
    @FXML
    private Label longestDistancetotal;
    @FXML
    private Label avgDistancetotal;
    @FXML
    private Label combinedDistancetotal;
    private MainLogic controller;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        controller = MainLogic.getInstance();
        mapDisplay.getEngine().load(getClass().getResource("index.html").toExternalForm());
        controller.setMap(mapDisplay);

    }

    @FXML
    private void newPacketButton(ActionEvent event) {
        try {
            //event that opens a window to packet creation
            FXMLLoader loadWindow = new FXMLLoader(getClass().getResource("windows/PacketCreationFXML.fxml"));
            Parent root = (Parent) loadWindow.load();
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            PacketCreationFXMLController a = loadWindow.getController();
            a.setStage(stage);
            a.setSmartPosts(controller.getSmartPostNames());
            stage.setScene(scene);
            stage.setAlwaysOnTop(true);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void drawSmartPosts(ActionEvent event) {
        //draw smartpost locations on the map based on a city
        String place = placeChoice.valueProperty().getValue().toUpperCase();
        controller.drawMultiplePosts(place);
    }

    @FXML
    private void deliveryButton(ActionEvent event) {
        try {
            //even that opens a window to delivery creation
            FXMLLoader loadWindow = new FXMLLoader(getClass().getResource("windows/DeliveryCreationFXML.fxml"));
            Parent root = (Parent) loadWindow.load();
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            DeliveryCreationFXMLController a = loadWindow.getController();
            a.setStage(stage);
            stage.setScene(scene);
            stage.setAlwaysOnTop(true);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void updatePlaces(MouseEvent event) {
        //even that updates cities when triggered
        placeChoice.getItems().clear();
        placeChoice.getItems().addAll(controller.getCities());
    }

    @FXML
    private void clearScreen(ActionEvent event) {
        //clear button event, removes all drawn markers form the map
        controller.clearMap();
    }

    @FXML
    private void switchToLog(Event event) {
        //update log info when switching to the log screen
        ArrayList<String> loginfo = controller.getLogInfo();
        createdPackets.setText(loginfo.get(0));
        storedPackets.setText(loginfo.get(1));
        sentPackets.setText(loginfo.get(2));
        deliveriesMade.setText(loginfo.get(3));
        packetsPerDelivery.setText(loginfo.get(4));
        shortestDistance.setText(loginfo.get(5));
        longestDistance.setText(loginfo.get(6));
        avgDistance.setText(loginfo.get(7));
        combinedDistance.setText(loginfo.get(8));
        createdPacketstotal.setText(loginfo.get(9));
        storedPacketstotal.setText(loginfo.get(10));
        sentPacketstotal.setText(loginfo.get(11));
        deliveriesMadetotal.setText(loginfo.get(12));
        packetsPerDeliverytotal.setText(loginfo.get(13));
        shortestDistancetotal.setText(loginfo.get(14));
        longestDistancetotal.setText(loginfo.get(15));
        avgDistancetotal.setText(loginfo.get(16));
        combinedDistancetotal.setText(loginfo.get(17));
    }

    public void refreshMap() {
        //reload the map file
        mapDisplay.getEngine().load(getClass().getResource("index.html").toExternalForm());
    }
}
