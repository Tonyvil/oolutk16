package timotei.dataClasses;

public class SmartPost {

    private String lng;
    private String lat;
    private String open;
    private String postCode;
    private String city;
    private String address;
    private String name;
    private int id;

    /**
     * @author Tony Vilpponen 0452774
     */
    /**
     * This is a holder object for all the Smart post data
     */
    public SmartPost(String lng, String lat, String name) {
        this.lng = lng;
        this.lat = lat;
        this.name = name;
        this.id = -1;
    }

    public SmartPost(String lng, String lat, String open, String postCode, String city, String address, String name) {
        this(lng, lat, name);
        this.open = open;
        this.postCode = postCode;
        this.city = city.toUpperCase();
        this.address = address;

    }

    public String getAddress() {
        return address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public String getOpen() {
        return open;
    }

    public String getPostCode() {
        return postCode;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name; //To change body of generated methods, choose Tools | Templates.
    }

}
