
package timotei.dataClasses;

/**
 *
 * This class contains information about items
 */
public class Item {

    private String name;
    private double weight;
    private double size;
    private boolean breakable;
    private int packageid;
    private int itemid;
    public Item(String name, double weight, double size, boolean breakable) {
        this.name = name;
        this.weight = weight;
        this.size = size;
        this.breakable = breakable;
        
    }

    public void setItemid(int itemid) {
        this.itemid = itemid;
    }
    public int getItemid() {
        return itemid;
    }
    public void setPackageid(int packageid) {
        this.packageid = packageid;
    }
    public String getName() {
        return name;
    }
    public int getPackageid() {
        return packageid;
    }
    public double getSize() {
        return size;
    }
    public double getWeight() {
        return weight;
    }
    public boolean isBreakable() {
        return breakable;
    }

    @Override
    public String toString() {
        return this.name + ", " + weight + "kg"; //To change body of generated methods, choose Tools | Templates.
    }

}
