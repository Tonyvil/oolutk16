/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei.windows;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import timotei.MainLogic;
import timotei.dataClasses.Item;

/**
 * Create items
 */
public class ItemCreationFXMLController implements Initializable {

    @FXML
    private TextField itemName;
    @FXML
    private TextField itemSize;
    @FXML
    private TextField itemWeight;
    @FXML
    private RadioButton breakableY;
    @FXML
    private RadioButton breakableN;
    @FXML
    private Button creationButton;
    private boolean wOK, sOK, nOK;
    private double weight, size;
    @FXML
    private ToggleGroup group;
    private Stage stage;
    private MainLogic controller;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        wOK = false;
        sOK = false;
        nOK = false;
        breakableN.setToggleGroup(group);
        breakableY.setToggleGroup(group);
        creationButton.setDisable(true);
        controller = MainLogic.getInstance();
    }

    @FXML
    private void CreateItem(ActionEvent event) { //creates the item
        String name = itemName.getText();
        controller.addItem(new Item(itemName.getText(), weight, size, breakableY.isSelected()));
        stage.close();
    }

    @FXML
    private void checkContentS(KeyEvent event) {
        try {//check if item size was inputted correctly
            size = Double.parseDouble(itemSize.getText());
            sOK = true;
        } catch (NumberFormatException e) {
            sOK = false;
        }
        checkInputs();
    }

    @FXML
    private void checkContentW(KeyEvent event) {
        try {//check if item weight was inputted correctly
            weight = Double.parseDouble((itemWeight.getText()));
            wOK = true;
        } catch (NumberFormatException e) {
            wOK = false;
        }
        checkInputs();
    }

    void setStage(Stage stage2) {
        this.stage = stage2;
    }

    @FXML
    private void checkContentN(KeyEvent event) {
        if (!itemName.getText().isEmpty()) {
            nOK = true;//check if the name was inputted
            checkInputs();
        } else {
            nOK = false;
        }
    }

    private void checkInputs() {//if everything is fine, enable item creation
        if (nOK && sOK && wOK) {
            creationButton.setDisable(false);
        } else {
            creationButton.setDisable(true);
        }
    }
}
