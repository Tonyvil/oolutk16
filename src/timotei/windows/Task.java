package timotei.windows;

import java.util.ArrayList;
import javafx.scene.Cursor;
import javafx.stage.Stage;
import javax.swing.SwingWorker;
import timotei.dataClasses.SmartPost;
import timotei.database.DatabaseHandler;
import timotei.database.SmartPostHandler;
import timotei.database.XmlController;

class Task extends SwingWorker<Void, Void> {
    /*
     * Main task. Executed in background thread. Loads smartposts and makes progressbar work
     */

    private Stage window;
    private LoadOptionFXMLController loadOption;

    public void setWindow(Stage window) {
        this.window = window;
    }

    public void setLoadOption(LoadOptionFXMLController loadOption) {
        this.loadOption = loadOption;
    }

    @Override
    public Void doInBackground() {
        ArrayList<SmartPost> boxList = new ArrayList();
        SmartPostHandler sph = SmartPostHandler.getInstance();
        DatabaseHandler sql = DatabaseHandler.getInstance();
        XmlController xml = new XmlController();
        if (sph.getSmartPostCount() != 0) {
            setProgress(100);
            return null;
        }
        int progress = 0;
        setProgress(0);
        xml.buildXML(xml.getXmlDocument());//load XML document
        progress += 20;
        setProgress(progress);
        firePropertyChange("progress", 0, progress);

        boxList.addAll(xml.parseXML());
        sql.addCity(boxList);//add cities
        progress += 20;
        setProgress(progress);
        firePropertyChange("progress", 20, progress);

        sql.addPostCode(boxList);
        progress += 20;//add postcodes
        setProgress(progress);
        firePropertyChange("progress", 40, progress);

        sql.addOpenHour(boxList);
        progress += 20;
        setProgress(progress);//add open hours
        firePropertyChange("progress", 60, progress);

        sql.addSmartBoxes(boxList);
        progress += 20;
        setProgress(progress);//add smartposts.
        firePropertyChange("progress", 80, progress);


        return null;
    }

    @Override
    public void done() {
        window.getScene().setCursor(Cursor.DEFAULT); //turn off the wait cursor
    }
}
