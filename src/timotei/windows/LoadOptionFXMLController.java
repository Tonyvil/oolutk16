/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei.windows;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import timotei.FXMLDocumentController;
import timotei.MainLogic;
/**
 *Initial Dialog window
 */
public class LoadOptionFXMLController implements Initializable, PropertyChangeListener {

    /**
     * Initializes the controller class.
     */
    private MainLogic controller;
    @FXML
    private Pane loadPane;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private Label loadInfo;
    @FXML
    private Pane promptPane;
    private Stage stage;
    @FXML
    private Button okButton;
    @FXML
    private Pane connectionFailedScreen;
    private Stage mainWindow;
    private FXMLDocumentController mainControl;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        okButton.setDisable(true);
        loadPane.setVisible(false);
        connectionFailedScreen.setVisible(false);
        controller = MainLogic.getInstance();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setMainWindow(Stage mainWindow) {
        this.mainWindow = mainWindow;
    }
  
    @FXML
    private void loadNewDatabase(ActionEvent event) {
        controller.emptyStorage();
        checkSmartposts();//deletes all unsent packages
        if(!loadPane.visibleProperty().get() && !connectionFailedScreen.visibleProperty().get()){
            mainWindow.show();//show the main window
            mainControl.refreshMap();//refresh the map to make it align properly
            stage.close();
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {//this updates the loadbar when loading smartposts
        if (evt.getPropertyName().equals("progress")) {
            Integer progress = (Integer) evt.getNewValue();
            double progressvalue = progress.doubleValue() / 100;
            progressBar.setProgress(progressvalue);
            if (progress == 100) {
                okButton.setDisable(false);
            }
        }
    }

    @FXML
    private void useOldDatabase(ActionEvent event) {
        checkSmartposts();//check if smartposts are loaded
        if(!connectionFailedScreen.visibleProperty().get()){
            mainWindow.show();//show the main window
            mainControl.refreshMap();
            stage.close();
        }
    }

    private void checkSmartposts() {//shows the loading screen for smartposts
        int count = controller.getSmartPostCount();
        if (count == 0) {
            promptPane.setVisible(false);
            loadPane.setVisible(true);
            stage.getScene().setCursor(Cursor.WAIT);
            Task task = new Task();
            task.addPropertyChangeListener(this);
            task.setLoadOption(this);
            task.setWindow(stage);
            task.execute();
        } else if(count == -1){
            promptPane.setVisible(false);
            connectionFailedScreen.setVisible(true);
        }
    }

    @FXML
    private void okEvent(ActionEvent event) {
        mainWindow.show();//close the window after loading smartposts.
        mainControl.refreshMap();
        stage.close();
    }

    @FXML
    private void shutDown(ActionEvent event) {
        System.exit(0); //close the app if database couldnt be connected
    }

    public void setFXMLDocumentController(Object controller) {
        mainControl = (FXMLDocumentController) controller;
    }

}
