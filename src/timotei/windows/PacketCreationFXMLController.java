/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei.windows;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import timotei.MainLogic;
import timotei.dataClasses.Item;
/**
 * Packet creation window
 */
public class PacketCreationFXMLController implements Initializable {

    @FXML
    private ComboBox<String> originSP;
    @FXML
    private ComboBox<String> destinationSP;
    @FXML
    private Label routeLen;
    @FXML
    private TextField recipientFN;
    @FXML
    private TextField recipientSN;
    @FXML
    private TextField recipientAddress;
    @FXML
    private ComboBox<Item> itemBox;
    @FXML
    private Label packetW;
    private Stage stage;
    @FXML
    private Pane pane2;
    @FXML
    private Pane pane3;
    @FXML
    private RadioButton tier1;
    @FXML
    private RadioButton tier2;
    @FXML
    private RadioButton tier3;
    private ToggleGroup buttongroup;
    @FXML
    private Label packetS;
    private ArrayList<Item> items;
    @FXML
    private Button createButton;
    private MainLogic controller;
    /**
     * Initializes the controller class.
     */
    public void setSmartPosts(ArrayList<String> posts) {
        originSP.getItems().addAll(posts);
        destinationSP.getItems().addAll(posts);

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        controller = MainLogic.getInstance();
        pane2.setDisable(true);
        pane3.setDisable(true);
        buttongroup = new ToggleGroup();
        tier1.setToggleGroup(buttongroup);
        tier2.setToggleGroup(buttongroup);
        tier3.setToggleGroup(buttongroup);
        itemBox.getItems().addAll(controller.loadItems());
        items = new ArrayList();
        tier2.setSelected(true);
    }

    @FXML
    private void addItem(ActionEvent event) {//add an item to the packet
        items.add(itemBox.valueProperty().getValue());
        double weight = controller.calculateWeight(items);
        double size = controller.calculateSize(items);
        packetW.setText(weight + " kg");
        packetS.setText(size + " l");
        if(size >= 50){
            tier2.setDisable(true);
            tier3.setSelected(true);
        }
        createButton.setDisable(false);
    }

    @FXML
    private void creationAction(ActionEvent event) {
        if (items.isEmpty()) {
            return;
        }
        int tier;
        if(tier1.isSelected()){
            tier = 1;
        } 
        else if (tier2.isSelected()){
            tier = 2;
        } else {
            tier = 3;
        }//create the packet
        controller.createPackage(items, tier, recipientFN.getText(), recipientSN.getText(), recipientAddress.getText(), destinationSP.getValue(), originSP.getValue());
        stage.close();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void postSelection(ActionEvent event) {
        if (originSP.getValue() != null && destinationSP.getValue() != null) {
            if (!originSP.getValue().equals(destinationSP.getValue())) {
                pane2.setDisable(false);
                //query the length of the route from the database
                //if not found ask map handler to do it and add it to the database
                double len = controller.getRouteLength(originSP.valueProperty().getValue(), destinationSP.valueProperty().getValue());
                routeLen.setText(len + " km");
                tier1.setDisable(len > 150);
                return;
            }
        }
        pane2.setDisable(true);
    }

    @FXML
    private void checkRecipient(KeyEvent event) {
        if (pane2.isDisable()) {
            pane3.setDisable(true);
        }//check if recipient info was input correctly
        if (!recipientAddress.getText().isEmpty()
                && !recipientFN.getText().isEmpty()
                && !recipientSN.getText().isEmpty()) {
            pane3.setDisable(false);
            return;
        }
        pane3.setDisable(true);
    }

    @FXML
    private void createItem(ActionEvent event) {
        try {//open the item creation window
            FXMLLoader loadWindow = new FXMLLoader(getClass().getResource("ItemCreationFXML.fxml"));
            Parent root = (Parent) loadWindow.load();
            Stage stage2 = new Stage();
            Scene scene = new Scene(root);
            ItemCreationFXMLController a = loadWindow.getController();
            a.setStage(stage2);
            stage2.setScene(scene);
            stage2.setAlwaysOnTop(true);
            stage2.show();
        } catch (IOException ex) {
            Logger.getLogger(PacketCreationFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onItemSelection(MouseEvent event) {//update the item list
        ArrayList<Item> tempItems = controller.loadItems();
        itemBox.getItems().clear();
        itemBox.getItems().addAll(tempItems);
    }

}
