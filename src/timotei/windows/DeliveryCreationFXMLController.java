
package timotei.windows;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import timotei.MainLogic;

/**
 * Delivery creation window
 */
public class DeliveryCreationFXMLController implements Initializable {

    private Stage stage;
    @FXML
    private ListView<String> packageList;
    @FXML
    private ComboBox<String> deliveryBox;
    @FXML
    private ComboBox<String> packageBox;
    private HashMap<String, Integer> routeinfo;
    private ArrayList<String> contents;
    private int routeid;
    private MainLogic controller;
    @FXML
    private Button sendButton;
    @FXML
    private Button addPackage;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        controller = MainLogic.getInstance();
        routeinfo = controller.getDeliveries();
        contents = new ArrayList();
        contents.clear();
        packageBox.setDisable(true);
        setDeliveryBox();
        
    }

    public void setStage(Stage stage) {
        this.stage = stage;//take a stage in that will be closed when we are done
    }

    private void setDeliveryBox() {
        //show deliveries in a box that user can choose from
        ArrayList<String> toDisplay = new ArrayList();
        for (String string : routeinfo.keySet()) {
            if (!controller.getPacketsByRoute(routeinfo.get(string), 0).isEmpty()) {
                toDisplay.add(string);
            }
        }

        if (!toDisplay.isEmpty()) {
            deliveryBox.getItems().addAll(toDisplay);
            deliveryBox.setValue(toDisplay.get(0));
            updatePackageList(new ActionEvent());
        } else {
            packageList.getItems().add("Lisää uusia paketteja ennen lähetyksen luomista.");
            contents = null;
        }

    }

    @FXML
    private void addToDelivery(ActionEvent event) {
        if (packageBox.getValue() == null) {
            return;
        }//add a package to a delivery, lock the delvery tier and remove the package from
        //the list of choosable packets
        contents.add(packageBox.getValue());
        String delivery = deliveryBox.getValue();
        routeid = routeinfo.get(delivery);
        System.out.println(routeid);
        deliveryBox.getItems().clear();
        deliveryBox.getItems().add(delivery);
        deliveryBox.setValue(delivery);
        updatePackageList(event);//fire an event to update the packagelist
        sendButton.setDisable(false);
    }

    @FXML
    private void sendDelivery(ActionEvent event) {
        int tier = Integer.parseInt(contents.get(0).split(" ")[3]);
        boolean itembreak = controller.sendDelivery(contents, tier, routeid);
        if (itembreak) {
            handleItemBreak();//send the delivery on its way and display notification
            //if items were broken.
        }
        stage.close();
    }

    @FXML
    private void updatePackageList(ActionEvent event) {//display packages in a certain route
        System.out.println("Delivery: " + deliveryBox.getValue() + " Contents: " + contents);
        if (deliveryBox.getValue() == null || contents == null) {
            return;
        }
        int tier = 0;
        if (!contents.isEmpty()) { //lock the tier of the delivery.
            tier = Integer.parseInt(contents.get(0).split(" ")[3]);
        }
        ArrayList<String> packages = controller.getPacketsByRoute(routeinfo.get(deliveryBox.getValue()), tier);
        packages.removeAll(contents);//remove the packages that have already been addeed from the list
        packageList.getItems().clear();//of choosable packets
        packageList.getItems().add("Numero, Nimi, Luokka");
        packageList.getItems().addAll(packages);
        packageBox.setDisable(false);
        packageBox.getItems().clear();
        packageBox.setValue(null);
        packageBox.getItems().addAll(packages);
        if (!packages.isEmpty()) {
            addPackage.setDisable(false);
        }
    }

    private void handleItemBreak() {
        try {//open the notification window for broken items
            FXMLLoader loadWindow = new FXMLLoader(getClass().getResource("ItemBreakFXML.fxml"));
            Parent root2 = (Parent) loadWindow.load();
            Stage stage2 = new Stage();
            Scene scene2 = new Scene(root2);
            ItemBreakFXMLController a = loadWindow.getController();
            a.setStage(stage2);
            stage2.setScene(scene2);
            stage2.initStyle(StageStyle.UNDECORATED);
            stage2.show();
        } catch (IOException ex) {
            Logger.getLogger(DeliveryCreationFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
