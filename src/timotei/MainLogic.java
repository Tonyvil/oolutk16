/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.web.WebView;
import timotei.dataClasses.Item;
import timotei.dataClasses.SmartPost;
import timotei.database.*;
import timotei.graphics.MapHandler;

/**
 *
 * @author Tony
 * This class acts as a communicator between GUI elements and the rest of the application logic
 */
public class MainLogic {

    public static MainLogic instance = null;
    private SmartPostHandler smartpostHandler;
    private MapHandler mh;
    private LogHandler lh;
    private QueryHandler qh;
    private DatabaseHandler dbh;
    private ItemHandler ih;

    private MainLogic() {
        smartpostHandler = SmartPostHandler.getInstance();
        mh = MapHandler.getInstance();
        ih = ItemHandler.getInstance();
        qh = new QueryHandler();
        dbh = DatabaseHandler.getInstance();
        lh = LogHandler.getInstance();
        ih.addItem(new Item("Tiili", 20, 12, true));
    }

    public static MainLogic getInstance() {
        if (instance == null) {
            instance = new MainLogic();
        }
        return instance;
    }

    ArrayList<String> getSmartPostNames() {
        return smartpostHandler.getSmartPostNames();
    }

    void setMap(WebView mapDisplay) {
        mh.setMap(mapDisplay);
    }

    ArrayList<SmartPost> getSmartPostsByCity(String place) {
        return smartpostHandler.getSmartPostsByCity(place);
    }

    void drawMultiplePosts(ArrayList<SmartPost> smartPostsByCity) {
        mh.drawMultiplePosts(smartPostsByCity);
    }

    void drawMultiplePosts(String place) {
        mh.drawMultiplePosts(smartpostHandler.getSmartPostsByCity(place));
    }

    ArrayList<String> getCities() {
        return smartpostHandler.getCities();
    }

    void clearMap() {
        mh.deletePosts();
        mh.deleteRoutes();
    }

    LogHandler getLog() {
        return lh;
    }

    public HashMap<String, Integer> getDeliveries() {
        return qh.getDeliveries();
    }

    public ArrayList<String> getPacketsByRoute(int routeID, int i) {
        return qh.getPacketsByRoute(routeID, i);
    }

    public boolean sendDelivery(ArrayList<String> contents, int tier, int routeid) {
        boolean itemBreak = dbh.sendDelivery(contents);
        smartpostHandler.drawRouteByRouteid(routeid, tier);
        dbh.writeLog(routeid);
        lh.makeDelivery(qh.getRouteLengthById(routeid));
        lh.sendPackets(contents.size());
        return itemBreak;
    }

    public void addItem(Item item) {
        ih.addItem(item);
    }

    public void emptyStorage() {
        smartpostHandler.emptyStorage();
    }

    public int getSmartPostCount() {
        return smartpostHandler.getSmartPostCount();
    }

    public double calculateWeight(ArrayList<Item> items) {
        return ih.calculateWeight(items);
    }

    public double calculateSize(ArrayList<Item> items) {
        return ih.calculateSize(items);
    }

    public ArrayList<Item> loadItems() {
        return ih.loadItems();
    }

    public void addPacket() {
        lh.addPacket();
    }

    public double getRouteLength(String origin, String destination) {
        return qh.getRouteLength(origin, destination);
    }

    public SmartPost getPostByName(String name) {
        return smartpostHandler.getPostByName(name);
    }

    public int queryPacketNumber() {
        return qh.getPacketNumber();
    }

    public void addItemsToPackage(int id, int packetID) {
        dbh.addItemsToPackage(id, packetID);
    }

    public void createPackage(ArrayList<Item> items, int tier, String firstName, String surname, String address, String destination, String origin) {
        ArrayList<Integer> itemids = new ArrayList();
        int packetID = queryPacketNumber();
        for (Item item : items) {
            addItem(item);
            if (!itemids.contains(item.getItemid())) {
                itemids.add(item.getItemid());
                addItemsToPackage(item.getItemid(), packetID);
            }
        }
        int recipientid = dbh.addRecipient(firstName, surname, address);
        int deliveryid = dbh.addDelivery(getPostByName(destination).getId(), getPostByName(origin).getId());
        dbh.addPackage(tier, recipientid, deliveryid);
        addPacket();
    }

    ArrayList<String> getLogInfo() {
        ArrayList<String> toReturn = new ArrayList();
        toReturn.add(lh.getCreatedPackets(false) + "");
        toReturn.add(lh.getStoredPackets(false) + "");
        toReturn.add(lh.getSentPackets(false) + "");
        toReturn.add(lh.getDeliveriesMade(false) + "");
        toReturn.add(lh.getPacketsDelivery(false) + "");
        toReturn.add(lh.getShortestDistance(false) + " km");
        toReturn.add(lh.getLongestDistance(false) + " km");
        toReturn.add(lh.getAverageDistance(false) + " km");
        toReturn.add(lh.getAllDistance(false) + " km");
        toReturn.add(lh.getCreatedPackets(true) + "");
        toReturn.add(lh.getStoredPackets(true) + "");
        toReturn.add(lh.getSentPackets(true) + "");
        toReturn.add(lh.getDeliveriesMade(true) + "");
        toReturn.add(lh.getPacketsDelivery(true) + "");
        toReturn.add(lh.getShortestDistance(true) + " km");
        toReturn.add(lh.getLongestDistance(true) + " km");
        toReturn.add(lh.getAverageDistance(true) + " km");
        toReturn.add(lh.getAllDistance(true) + " km");
        return toReturn;
    }
}
